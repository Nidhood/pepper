# Nao Control Application

Project about nao bringup and simple application using docker

## Getting Started 

1. Clone the repo
   ```sh
   git clone https://gitlab.com/IEEERASJaveriana/DockersforROS/ROSIndigo.git
   ```
2. Checkout to main-lite
   ```sh
   git checkout main-Lite
   ```
3. go to ROSIndigo folder
   ```sh
   cd ROSIndigo
   ```
4. let the bash files as executables 
   ```
   chmod +x run.sh run_emptyVolume.sh 
   ```
5. If it is your first time run 
   ```
   ./run_emptyVolume.sh 
   ```
   Otherwise (when you want to run your app in multiples terminals)
   ```
   ./run.sh 
   ```
6. In your docker go to nao_ws:
   ```
   cd nao_ws 
   ```
7. build nao_ws:
   ```
   catkin_make 
   ```
8. update your workspace:
   ```
   source devel/setup.bash
   ```
9. Use your nao robot to verify the robot ip and modify the param in the launch files
10. Now you can run any nao launch, for example:
   ```
   roslaunch paquete_nao nao.launch
   ```
