#!/bin/bash

sudo apt-get install ros-indigo-pepper-.*

cd nao_ws
catkin_make
source /opt/ros/indigo/setup.bash
source ~/app/nao_ws/devel/setup.bash

cd /src
git clone --recursive https://github.com/bambocher/pocketsphinx-python
cd pocketsphinx-python
python setup.py install
sudo apt-get install python-pip
sudo apt-get install python-sphinx
sudo apt upgrade
