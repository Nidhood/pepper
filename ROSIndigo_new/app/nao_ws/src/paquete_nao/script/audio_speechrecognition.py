#! /usr/bin/env python
# -*- encoding: UTF-8 -*-

"""Example: A Simple class to get & read FaceDetected Events"""

import qi
import time
import sys
import argparse
from naoqi import ALProxy

class SpeechRecognition(object):
    """
    A simple class to react to face detection events.
    """

    def __init__(self, app):
        """
        Initialisation of qi framework and event detection.
        """
        super(SpeechRecognition, self).__init__()
        app.start()
        session = app.session
        # Get the service ALMemory.
        self.memory = session.service("ALMemory")
        # Connect the event callback.
        #self.speech = False
        self.subscriber = Recognition.subscriber("WordRecognized")
        self.subscriber.signal.connect(self.on_human_listened)
        # Get the services ALAnimatedSpeech and ALSpeechRecognition.
        self.tts = tts
        self.speech_detection = speech_detection
        self.speech_detection.setLanguage('English')
        vocabulary = ["yes", "no", "please","hello", "bye"]
        self.speech_detection.pause(True)
        #self.speech_detection.setVocabulary(vocabulary, False)
        self.speech_detection.setAudioExpression(True)
        self.speech_detection.pause(False)
        self.speech_detection.subscribe("SpeechRecognition")

    def on_human_listened(self, value):
        """
        Callback for event SpeechDetected.
        """
        if value != []:  # empty value when the face disappears
            #self.speech = False
          #  pass
        #elif not self.speech:  # only speak the first time a face appears
         #   self.speech = True
            print("I listen a voice!")
            print(value[0])
            if value[0] == 'hello':
                print(value)
                value = []
                self.tts.say("hello! ^start(animations/Stand/Gestures/Hey_1) Encatado de conocerte")
            elif value[0] == 'bye':
                value = []
                print(value)
                self.tts.say("bye! ^start(animations/Stand/Gestures/Hey_1) Nos vemos pronto") 
            # First Field = TimeStamp.
            #timeStamp = value[0]
            #print("TimeStamp is: " + str(timeStamp))

            # Second Field = array of face_Info's.
            # faceInfoArray = value[1]
            # for j in range( len(faceInfoArray)-1 ):
            #     faceInfo = faceInfoArray[j]

            #     # First Field = Shape info.
            #     faceShapeInfo = faceInfo[0]

            #     # Second Field = Extra info (empty for now).
            #     faceExtraInfo = faceInfo[1]

            #     print("Face Infos :  alpha %.3f - beta %.3f" % (faceShapeInfo[1], faceShapeInfo[2]))
            #     print("Face Infos :  width %.3f - height %.3f" % (faceShapeInfo[3], faceShapeInfo[4]))
            #     print("Face Extra Infos :" + str(faceExtraInfo))
            

    def run(self):
        """
        Loop on, wait for events until manual interruption.
        """
        print("Starting SpeechRecognition")
        try:
            while True:
                #time.sleep(1)
                pass
        except KeyboardInterrupt:
            print("Interrupted by user, stopping SpeechRecognition")
            self.speech_detection.unsubscribe("SpeechRecognition")
            #stop
            sys.exit(0)


if __name__ == "__main__":
    # parser = argparse.ArgumentParser()
    # parser.add_argument("--ip", type=str, default="192.168.1.102",
    #                     help="Robot IP address. On robot or Local Naoqi: use '192.168.0.103'.")
    # parser.add_argument("--port", type=int, default=9559,
    #                     help="Naoqi port number")

    # args = parser.parse_args()
    # try:
    #     # Initialize qi framework.
    #     connection_url = "tcp://" + args.ip + ":" + str(args.port)
    #     app = qi.Application(["SpeechRecognition", "--qi-url=" + connection_url])
    # except RuntimeError:
    #     print ("Can't connect to Naoqi at ip \"" + args.ip + "\" on port " + str(args.port) +".\n"
    #            "Please check your script arguments. Run with -h option for help.")
    #     sys.exit(1)
    ip = "192.168.1.102"
    Recognition = ALProxy("ALMemory", ip , 9559)
    tts = ALProxy("ALAnimatedSpeech", ip , 9559)
    speech_detection=ALProxy("ALSpeechRecognition", ip, 9559)
    human_greeter = SpeechRecognition(app)
    human_greeter.run()