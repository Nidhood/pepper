#!/usr/bin/env python

from naoqi import ALProxy
import rospy

global shoulder_roll_limit
shoulder_roll_limit = 0.3

def move_arms(data):
	#print ((1-data.axes[2])/2.0)
	joints = ['LShoulderPitch', 'RShoulderPitch', 'LShoulderRoll', 'RShoulderRoll', 'LElbowRoll', 'RElbowRoll', 'LElbowYaw', 'RElbowYaw']
	global shoulder_roll_limit
	if(data[3] < 0.0):
		shoulder_roll_limit = 0.3
	else:
		shoulder_roll_limit = 1.3
	angles = [-2.0*data[1], -2.0*data[1], shoulder_roll_limit*data[3], -shoulder_roll_limit*data[3], ((1-data[2])/2.0)*(-1.5), ((1-data[2])/2.0)*(1.5), (1-data[5])*data[5], -(1-data[5])*data[5]]
	#print angles
	motion.angleInterpolationWithSpeed(joints, angles, 1.0)
print("Hey")
motion = ALProxy('ALMotion', '198.162.1.100',9560)
#11311
motion.setStiffnesses('Body',1.0)
rospy.init_node('nao_joy_controller')

#move_arms([1,0,0,0,0])

#rospy.spin()
