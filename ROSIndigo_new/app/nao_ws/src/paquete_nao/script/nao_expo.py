#!/usr/bin/env python
import rospy
from naoqi import ALProxy

from geometry_msgs.msg import Twist, Pose
import sys, select, termios, tty
import numpy as np
from sensor_msgs.msg import Range

nombres_joints = ['HeadYaw', 'HeadPitch', 'LShoulderPitch', 'LShoulderRoll',
                 'LElbowYaw', 'LElbowRoll', 'LWristYaw', 'LHand', 'LHipYawPitch',
                 'LHipRoll', 'LHipPitch', 'LKneePitch', 'LAnklePitch', 'LAnkleRoll', 
                 'RHipYawPitch', 'RHipRoll', 'RHipPitch', 'RKneePitch', 'RAnklePitch',
                  'RAnkleRoll', 'RShoulderPitch', 'RShoulderRoll', 'RElbowYaw', 'RElbowRoll',
                   'RWristYaw', 'RHand']

nombres_brazos = [ 'RShoulderPitch', 'RShoulderRoll','RElbowYaw', 'RElbowRoll', 'RWristYaw',
                    'LShoulderPitch', 'LShoulderRoll','LElbowYaw', 'LElbowRoll', 'LWristYaw']

nombres_brazo_D = [ 'RShoulderPitch', 'RShoulderRoll','RElbowYaw', 'RElbowRoll', 'RWristYaw']

def guardar_pose(motion,nombre_archivo, partes):

    angulos = np.array(motion.getAngles(partes,True))
    print(angulos)
    with open('/workspaces/Ros_inicio/Docker_indigo/Archivos_nao/src/paquete_nao/script/'+nombre_archivo+'.npy', 'wb') as f:
        np.save(f, angulos)

def leer_pose(nombre_archivo):
    with open('/workspaces/Ros_inicio/Docker_indigo/Archivos_nao/src/paquete_nao/script/'+nombre_archivo+'.npy', 'rb') as f:
        angulos = np.load(f)
        return angulos

def poner_pose(motion,angulos, partes,vel):

    motion.setStiffnesses(partes, 1.0)
    #motion.setAngles(nombres_joints,angulos,0.1)
    motion.angleInterpolationWithSpeed(partes,angulos,vel)

def desrigidizar(motion, parte):
    #motion.setStiffnesses("Body", 0.0)
    motion.setStiffnesses(parte, 0.0)


def poses(post,postura):
    ##   pose inicial, Acostado boca arriba, acostado boca abajo, sentado, sentado con brazos atras, parado, parado un poco agachado, parado con brazos al frente
    posturas = ["Crouch","LyingBack","LyingBelly","Sit","SitRelax","Stand","StandInit","StandZero"]
    postura.goToPosture(posturas[post],1)


def hablar_V2(tts,palabra):
    tts.say(palabra)

def getKey():
    tty.setraw(sys.stdin.fileno())
    rlist, _, _ = select.select([sys.stdin], [], [], 0.1)
    if rlist:
        key = sys.stdin.read(1)
    else:
        key = ''

    termios.tcsetattr(sys.stdin, termios.TCSADRAIN, settings)
    return key

cont_1 = 0
cont_2= 0
flag_1 = 0
flag_2 = 0

def sonar_callback_R(data):
    global estado, flag_1, tts,cont_1
    #print(cont_1)
    #print(data.range)
    if(control_speed > 0):
        if(data.range<0.45):
            cont_1=cont_1+1
        else:
            cont_1 = 0
            
        if(cont_1>= 5):
            cont_1 = 5
            flag_1 = 1
            estado = 0
            hablar_V2(tts,"Oh no, an obstacule")
    else:
        if(data.range>0.45):
            flag_1 = 0
            cont_2 = 0





def sonar_callback_L(data):
    global estado,flag_2,tts,cont_2
    if(control_speed > 0):
        if(data.range<0.45):
            cont_2=cont_2+1
        else:
            cont_2 = 0
            flag_2 = 0
        if(cont_2>= 5):
            cont_2 = 5
            flag_2 = 1
            estado = 0
            hablar_V2(tts,"Oh no, an obstacule")
    else:
        if(data.range>0.45):
            flag_2 = 0
            cont_2 = 0

if __name__=="__main__":
    rospy.init_node("nao_expo")
    ip = rospy.get_param("/nao_expo/ip_nao")
    motion = ALProxy("ALMotion", ip , 9559)
    postura = ALProxy("ALRobotPosture", ip, 9559)
    tts = ALProxy("ALTextToSpeech", ip, 9559)
    motion.wbEnable(True)
    poses(6,postura)

    #rospy.Subscriber('/nao_robot/sonar/right/sonar',Range,sonar_callback_R)
    #rospy.Subscriber('/nao_robot/sonar/left/sonar',Range,sonar_callback_L)

    control_speed = 0
    velocidad = 1
    vel_ang = 0.1
    max_vel_ang= 0.5

    settings = termios.tcgetattr(sys.stdin)
    pub = rospy.Publisher('/cmd_vel', Twist, queue_size=10)
    control_speed = 0
    control_turn = 0

    tiempo =[0,0] ## pos 0 segundo, pos 1 microsegundo
    estado = 0 ## el estado 0 indica estatico, el estado 1 indica adelante, el estado 2 indica atras

    try:
        print("comenzando...")
        while(1):
            key = getKey()

            if((flag_1 ==1) or (flag_2==1)):
                estado = 0
                control_speed= 0
            elif(key =='w'):
                if(estado == 5):
                    poses(6,postura)
                estado =1
                control_speed= velocidad
                control_turn=0
            elif(key=='s'):
                if(estado == 5):
                    poses(6,postura)
                estado =2
                control_turn=0
                control_speed= -velocidad
            elif(key=='a'):
                if(estado == 5):
                    poses(6,postura)
                if(estado == 1):
                    estado =3
                    control_turn=vel_ang
                elif(estado ==2):
                    estado =3
                    control_turn=-vel_ang
                elif(estado == 4):
                    estado =3
                    if(control_speed == velocidad or control_speed== 0):
                        control_turn=vel_ang
                    else:
                        control_turn=-vel_ang
                else:
                    estado =3
                    if(control_speed == velocidad or control_speed== 0):
                        if(control_turn<= max_vel_ang):
                            control_turn+=vel_ang
                    else:
                        if(control_turn> -max_vel_ang):
                            control_turn-=vel_ang
            elif(key=='d'):
                if(estado == 5):
                    poses(6,postura)
                if(estado == 1):
                    estado =4
                    control_turn=-vel_ang
                elif(estado ==2):
                    estado =4
                    control_turn=vel_ang
                elif(estado == 3):
                    estado = 4
                    if(control_speed == velocidad or control_speed== 0):
                        control_turn=vel_ang
                    else:
                        control_turn=-vel_ang
                else:
                    estado = 4
                    if(control_speed == velocidad or control_speed== 0):
                        if(control_turn > -max_vel_ang):
                            control_turn+=-vel_ang
                    else:
                        if(control_turn<= max_vel_ang):
                            control_turn+=vel_ang
            elif(key=='z' or (flag_1==1 or flag_2==1)):
                if(estado == 5):
                    poses(6,postura)
                estado = 0
                control_turn=0
                control_speed= 0
            elif(key=='y'):
                control_speed = 0
                motion.move(0, 0,0)
                estado = 0
                poses(6,postura)
                motion.openHand("RHand")
                hablar_V2(tts,"Hello")
                for i in range(3):
                    angulos = (leer_pose("brazo_D_0").tolist())
                    poner_pose(motion,angulos,nombres_brazo_D,0.5)

                    angulos = (leer_pose("brazo_D_1").tolist())
                    poner_pose(motion,angulos,nombres_brazo_D,0.5)

                motion.closeHand("RHand")
                angulos = (leer_pose("brazo_D_2").tolist())
                poner_pose(motion,angulos,nombres_brazos,0.3)
                hablar_V2(tts,"I am nao")
                motion.openHand("RHand")
                motion.openHand("LHand")
                angulos = (leer_pose("brazo_D_3").tolist())
                poner_pose(motion,angulos,nombres_brazos,0.3)
                hablar_V2(tts,"And this is Ras")
                motion.closeHand("RHand")
                motion.closeHand("LHand")
                poses(6,postura)
            elif (key== 'u'):
                if(estado != 5):
                    motion.move(0, 0,0)
                estado = 5
                hablar_V2(tts,"I can also rest a bit")
                poses(1,postura)
            elif (key== 'i'):
                if(estado != 5):
                    control_speed= 0
                    motion.move(0, 0,0)
                estado = 5
                hablar_V2(tts,"I can sit and talk with you")
                poses(4,postura)
            elif (key== 'o'):
                hablar_V2(tts,"I am walking")
            elif (key == '\x03'):
                break
            elif(key != ''):
                control_speed=0
                control_turn=0
                estado =0



            motion.move(control_speed, 0,control_turn)

            print('velocidad: ',control_speed, '  giro: ',control_turn , " estado = " , estado)



    except Exception as e:
        print(e)

    finally:
        motion.move(0, 0,0)


    termios.tcsetattr(sys.stdin, termios.TCSADRAIN, settings)
