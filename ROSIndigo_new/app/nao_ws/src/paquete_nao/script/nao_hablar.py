#!/usr/bin/env python
import rospy
import cv2
from sensor_msgs.msg import Image
import numpy as np
from naoqi import ALProxy
import sys

def hablar(ip):
    tts = ALProxy("ALTextToSpeech", ip, 9559)
    palabra = ""
    for i in range(len(sys.argv)-1):
        palabra+= sys.argv[i+1]+" "
    tts.say(palabra)


def cambio_color(ip):
    proxy = ALProxy("ALLeds", ip, 9559)
    re1=[
        "FaceLedRight0",
        "FaceLedRight1",
        "FaceLedRight2",
        "FaceLedRight3",
        "FaceLedRight4",
        "FaceLedRight5",
        "FaceLedRight6",
        "FaceLedRight7",
    ]
    re2=[
        "FaceLedLeft0",
        "FaceLedLeft1",
        "FaceLedLeft2",
        "FaceLedLeft3",
        "FaceLedLeft4",
        "FaceLedLeft5",
        "FaceLedLeft6",
        "FaceLedLeft7",
    ]
    proxy.createGroup("RightEye1",re1)
    proxy.createGroup("RightEye2",re2)
    color_r = [255,255,0]
    color_l = [0,255,255]
    proxy.fadeRGB('RightEye1',color_r[0]/255, color_r[1]/255,  color_r[2]/255,1)
    proxy.fadeRGB('RightEye2',color_l[0]/255, color_l[1]/255,  color_l[2]/255,1)


def funcion_callback(data):
    h = (data.height)
    w = (data.width)
    camara = (np.fromstring(data.data, np.uint8)).reshape(h,w,3)
    cv2.imshow("Imagen",camara)
    cv2.waitKey(10)

def no_rigid(ip):
    motion = ALProxy("ALMotion", ip, 9559)
    motion.setStiffnesses("Body", 0.0)

def caminar(ip):
    motion = ALProxy("ALMotion", ip, 9559)
    motion.setStiffnesses("Body", 0.0)
    motion.setStiffnesses("Body", 1.0)
    motion.moveInit()
    motion.moveTo(1, 0, 0)

def poses(post, ip):
    postura = ALProxy("ALRobotPosture", ip, 9559)
    posturas = ["Crouch","LyingBack","LyingBelly","Sit","SitRelax","Stand","StandInit","StandZero"]
    postura.goToPosture(posturas[post],1)
    hablar_V2("Oh yeah")
    print("Ready")

def hablar_V2(palabra, ip):
    tts = ALProxy("ALTextToSpeech", ip, 9559)
    tts.say(palabra)



if __name__=="__main__":

    rospy.init_node("nao_hablar", anonymous=False)
    ip = rospy.get_param("/nao_hablar/ip_nao")
    #rospy.Subscriber("/nao_robot/camera/top/camera/image_raw", Image, funcion_callback)
    print("Trabajando")
    # spin() simply keeps python from exiting until this node is stopped

    hablar(ip)
    for i in range(7):
        a=1
    caminar(ip)
        #poses(i, ip)
    #caminar(ip)
    rospy.spin()
    
