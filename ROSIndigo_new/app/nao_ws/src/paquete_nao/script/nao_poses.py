#!/usr/bin/env python
import rospy
import cv2
from sensor_msgs.msg import Image
import numpy as np
from naoqi import ALProxy
import sys


nombres_joints = ['HeadYaw', 'HeadPitch', 'LShoulderPitch', 'LShoulderRoll',
                 'LElbowYaw', 'LElbowRoll', 'LWristYaw', 'LHand', 'LHipYawPitch',
                 'LHipRoll', 'LHipPitch', 'LKneePitch', 'LAnklePitch', 'LAnkleRoll', 
                 'RHipYawPitch', 'RHipRoll', 'RHipPitch', 'RKneePitch', 'RAnklePitch',
                  'RAnkleRoll', 'RShoulderPitch', 'RShoulderRoll', 'RElbowYaw', 'RElbowRoll',
                   'RWristYaw', 'RHand']

nombres_brazos = [ 'RShoulderPitch', 'RShoulderRoll','RElbowYaw', 'RElbowRoll', 'RWristYaw',
                    'LShoulderPitch', 'LShoulderRoll','LElbowYaw', 'LElbowRoll', 'LWristYaw']

nombres_brazo_D = [ 'RShoulderPitch', 'RShoulderRoll','RElbowYaw', 'RElbowRoll', 'RWristYaw']

def guardar_pose(motion,nombre_archivo, partes):

    angulos = np.array(motion.getAngles(partes,True))
    print(angulos)
    with open('~/app/nao_ws/src/paquete_nao/script/'+nombre_archivo+'.npy', 'wb') as f:
        np.save(f, angulos)

def leer_pose(nombre_archivo):
    with open('~/app/nao_ws/src/paquete_nao/script/'+nombre_archivo+'.npy', 'rb') as f:
        angulos = np.load(f)
        return angulos

def poner_pose(motion,angulos, partes,vel):

    motion.setStiffnesses(partes, 1.0)
    #motion.setAngles(nombres_joints,angulos,0.1)
    motion.angleInterpolationWithSpeed(partes,angulos,vel)

def desrigidizar(motion, parte):
    #motion.setStiffnesses("Body", 0.0)
    motion.setStiffnesses(parte, 0.0)


def poses(post,postura):
    ##   pose inicial, Acostado boca arriba, acostado boca abajo, sentado, sentado con brazos atras, parado, parado un poco agachado, parado con brazos al frente
    posturas = ["Crouch","LyingBack","LyingBelly","Sit","SitRelax","Stand","StandInit","StandZero"]
    postura.goToPosture(posturas[post],1)


def hablar_V2(tts,palabra):
    tts.say(palabra)

if __name__=="__main__":
    rospy.init_node("nao_poses")
    ip = rospy.get_param("/nao_poses/ip_nao")
    postura = ALProxy("ALRobotPosture", ip, 9559)
    motion = ALProxy("ALMotion", ip , 9559)
    tts = ALProxy("ALTextToSpeech", ip, 9559)
    motion.wbEnable(True)
    #brazos_arriba

    #partes : nombres_brazos,nombres_joints
    #guardar_pose(motion,"brazo_D_3",nombres_brazos)

    
    poses(6,postura)
    motion.openHand("RHand")
    hablar_V2(tts,"Hello")
    for i in range(3):
        angulos = (leer_pose("brazo_D_0").tolist())
        poner_pose(motion,angulos,nombres_brazo_D,0.5)

        angulos = (leer_pose("brazo_D_1").tolist())
        poner_pose(motion,angulos,nombres_brazo_D,0.5)

    motion.closeHand("RHand")
    angulos = (leer_pose("brazo_D_2").tolist())
    print(angulos)
    poner_pose(motion,angulos,nombres_brazos,0.3)
    hablar_V2(tts,"I am nao")
    motion.openHand("RHand")
    motion.openHand("LHand")
    angulos = (leer_pose("brazo_D_3").tolist())
    poner_pose(motion,angulos,nombres_brazos,0.3)
    motion.closeHand("RHand")
    motion.closeHand("LHand")
    poses(6,postura)

    hablar_V2(tts,"I can walk too")
    motion.move(1, 0,0)
    # parte: nombres_brazos, "Body"
    #desrigidizar(motion, nombres_brazos)


    



