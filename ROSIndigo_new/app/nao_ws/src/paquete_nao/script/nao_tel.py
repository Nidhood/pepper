#!/usr/bin/env python
import rospy
from naoqi import ALProxy

from geometry_msgs.msg import Twist, Pose
from datetime import datetime
import sys, select, termios, tty


def getKey():
    tty.setraw(sys.stdin.fileno())
    rlist, _, _ = select.select([sys.stdin], [], [], 0.1)
    if rlist:
        key = sys.stdin.read(1)
    else:
        key = ''

    termios.tcsetattr(sys.stdin, termios.TCSADRAIN, settings)
    return key

def dar_tiempo():
    fecha = datetime.now()
    seg = fecha.second
    mic = fecha.microsecond
    return seg,mic



if __name__=="__main__":
    velocidad = 1
    vel_ang = 0.1
    max_vel_ang= 0.5
    tiempo_espera=0.01 ## en segundos

    settings = termios.tcgetattr(sys.stdin)
    rospy.init_node("control_nao")
    ip = rospy.get_param("/control_nao/ip_nao")
    motion = ALProxy("ALMotion", ip , 9559)
    pub = rospy.Publisher('/cmd_vel', Twist, queue_size=10)
    control_speed = 0
    control_turn = 0

    tiempo =[0,0] ## pos 0 segundo, pos 1 microsegundo
    estado = 0 ## el estado 0 indica estatico, el estado 1 indica adelante, el estado 2 indica atras

    try:
        print("comenzando...")
        while(1):
            key = getKey()

            ##if(key != ''):
            ##    print(key)

            if(key =='w'):
                print("comando")
                print(key)
                estado =1
                control_speed= velocidad
                control_turn=0
                seg_late,mic_late=dar_tiempo()
            elif(key=='s'):
                print("comando")
                print(key)
                estado =2
                control_turn=0
                control_speed= -velocidad
                seg_late,mic_late=dar_tiempo()
            elif(key=='a'):
                print("comando")
                print(key)
                if(estado == 1):
                    estado =3
                    control_turn=vel_ang
                    seg_late,mic_late=dar_tiempo()
                elif(estado ==2):
                    estado =3
                    control_turn=-vel_ang
                    seg_late,mic_late=dar_tiempo()
                elif(estado == 4):
                    estado =3
                    if(control_speed == velocidad or control_speed== 0):
                        control_turn=vel_ang
                    else:
                        control_turn=-vel_ang
                    seg_late,mic_late=dar_tiempo()
                else:
                    estado =3
                    if(control_speed == velocidad or control_speed== 0):
                        if(control_turn<= max_vel_ang):
                            control_turn+=vel_ang
                    else:
                        if(control_turn> -max_vel_ang):
                            control_turn-=vel_ang
                    seg_late,mic_late=dar_tiempo()
            elif(key=='d'):
                if(estado == 1):
                    estado =4
                    control_turn=-vel_ang
                    seg_late,mic_late=dar_tiempo()
                elif(estado ==2):
                    estado =4
                    control_turn=vel_ang
                    seg_late,mic_late=dar_tiempo()
                elif(estado == 3):
                    estado = 4
                    if(control_speed == velocidad or control_speed== 0):
                        control_turn=vel_ang
                    else:
                        control_turn=-vel_ang
                    seg_late,mic_late=dar_tiempo()
                else:
                    estado = 4
                    if(control_speed == velocidad or control_speed== 0):
                        if(control_turn > -max_vel_ang):
                            control_turn+=-vel_ang
                    else:
                        if(control_turn<= max_vel_ang):
                            control_turn+=vel_ang
                    seg_late,mic_late=dar_tiempo()
            elif(key=='z'):
                estado = 0
                control_turn=0
                control_speed= 0
                seg_late,mic_late=dar_tiempo()
            elif (key == '\x03'):
                break
            elif(key != ''):
                control_speed=0
                control_turn=0
                estado =0

            if(estado != 0):
                seg_act,mic_act=dar_tiempo()
                if(seg_act<seg_late):
                    seg_act= seg_act+60
                hora_actual = seg_act+mic_act/1000000
                hora_late = seg_late+mic_late/1000000
                #if(hora_actual-hora_late>tiempo_espera):
                #    estado = 0
                #    control_speed=0
                #    control_turn=0


            motion.move(control_speed, 0,control_turn)

            #print('velocidad: ',control_speed, '  giro: ',control_turn , " estado = " , estado)



    except Exception as e:
        print(e)

    finally:
        motion.move(0, 0,0)


    termios.tcsetattr(sys.stdin, termios.TCSADRAIN, settings)
