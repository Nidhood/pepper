#!/usr/bin/env python

import numpy as np 
import rospy

from sensor_msgs.msg import Range

import cv2 
import sys

def sonar_callback_R(data):
    print("En el derecho es:",data.range)
    if(data.range<0.28):
        print("Error")

def sonar_callback_L(data):
    print("En el izquierdo es:",data.range)
    if(data.range<0.28):
        print("Error")



if __name__=="__main__":
    rospy.init_node("read_sonar", anonymous=False)
    rospy.Subscriber('/nao_robot/sonar/right/sonar',Range,sonar_callback_R)
    rospy.Subscriber('/nao_robot/sonar/left/sonar',Range,sonar_callback_L)
    print("Trabajando")
    rospy.spin()
