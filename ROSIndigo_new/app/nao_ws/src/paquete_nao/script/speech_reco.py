#!/usr/bin/env python
import rospy
from naoqi import ALProxy
import time

if __name__=="__main__":
    rospy.init_node("speech_reco", anonymous=False)
    ip = rospy.get_param("/speech_reco/ip_nao")
    # Creates a proxy on the speech-recognition module
    asr = ALProxy("ALSpeechRecognition", ip, 9559)

    asr.setLanguage("English")

    # Example: Adds "yes", "no" and "please" to the vocabulary (without wordspotting)
    vocabulary = ["yes", "no", "please"]
    asr.setVocabulary(vocabulary, False)

    # Start the speech recognition engine with user Test_ASR
    asr.subscribe("Test_ASR")
    print 'Speech recognition engine started'
    WordRecognized()
    time.sleep(20)
    asr.unsubscribe("Test_ASR")
