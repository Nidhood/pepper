import qi
import sys
import argparse


def main(argv):
    if len(argv) == 2:
        parser = argparse.ArgumentParser()
	    parser.add_argument("--ip", type=str, default="192.168.1.100")
	    parser.add_argument("--text", type=str)
	    args = parser.parse_args()

        try:
            session = qi.Session()
            try:
                session.connect("tcp://" + args.ip + ":9559")
            except RuntimeError:
                print "{\"out\":\"error\"}"
                sys.exit(1)

            tts = session.service("ALTextToSpeech")

            tts.say(argv[1])
            print "{\"out\":\"ok\"}"
        except:
            print "{\"out\":\"error\"}"
    else:
        print "{\"out\":\"no parameter specified, expected text to speak\"}"


if __name__ == "__main__":
    main(sys.argv)

