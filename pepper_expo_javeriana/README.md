# Pepper setup configutatoin

Project about pepper bringup and simple application using docker

## Getting Started 

1. Configure master uri:
   ```sh
   export ROS_MASTER_URI=http://<here goes your ip>:11311
   export ROS_IP=<here goes your ip>
   ```
2. Mount your docker conteiner:
   ```sh
   cd pepper_expo_javeriana/run_emptyVolume.sh
   ```
3. Install dependences:
   ```sh
   source init.bash
   ```
4. Configure your ip:
   ```
   nano nao_ws/src/peppervoice/launch/hi_expo.launch
   ```
   Then
   ```
   nano nao_ws/src/peppervoice/launch/ras_information_expo.launch
   ```
5. Run your launch of preference:
   ```
   roslaunch peppervoice hi_expo
   ```
   Otherwise 
   ```
   roslaunch peppervoice ras_information_expo 
   ```
