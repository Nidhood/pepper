#!/bin/bash

sudo apt-get install ros-indigo-pepper-.*
cd nao_ws
catkin_make
source /opt/ros/indigo/setup.bash
source ~/app/nao_ws/devel/setup.bash
sudo apt upgrade
