FROM osrf/ros:indigo-desktop-full


ARG uid=1001
ARG gid=51


RUN apt-get update && apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 4B63CF8FDE49746E98FA01DDAD19BAB3CBF125EA && \
    echo "deb http://snapshots.ros.org/indigo/final/ubuntu trusty main" > /etc/apt/sources.list.d/ros1-snapshots.list && \
    apt-get install -y ros-indigo-driver-base ros-indigo-move-base-msgs ros-indigo-octomap && \
    apt-get install -y ros-indigo-octomap-msgs ros-indigo-humanoid-msgs ros-indigo-humanoid-nav-msgs && \
    apt-get install -y ros-indigo-camera-info-manager ros-indigo-camera-info-manager-py && \
    apt-get install -y nano

RUN addgroup --gid $gid mygroup \
    && adduser --disabled-password --uid $uid --gid $gid nao && \
    echo "nao ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers && \
    chmod 0440 /etc/sudoers && \
    chmod g+w /etc/passwd 


USER nao

WORKDIR home/nao/app

COPY --chown=nao:mygroup  app/ .

RUN chmod 774 ~/app/nao_ws/

RUN /bin/bash -c "source /opt/ros/indigo/setup.bash;"

ENV PYTHONPATH /home/nao/app/naoqi/pynaoqi-python2.7-2.1.4.13-linux64:$PYTHONPATH
