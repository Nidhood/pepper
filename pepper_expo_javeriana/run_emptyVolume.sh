#!/bin/bash

docker build -f build/pepper.Dockerfile -t naoimage .
docker volume rm naoVol
docker run --rm -it \
	--env="DISPLAY" \
    	--env="QT_X11_NO_MITSHM=1" \
    	--volume="/tmp/.X11-unix:/tmp/.X11-unix:rw" \
	--network host \
	-v naoVol:/app \
	--device /dev/snd \
	naoimage:latest

